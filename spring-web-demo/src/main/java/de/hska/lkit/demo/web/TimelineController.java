package de.hska.lkit.demo.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TimelineController {

	@RequestMapping(value = "/timeline")
	public String timelineSubmit(@ModelAttribute Timeline timeline, Model model) {
		model.addAttribute("timeline", timeline != null ? timeline : new Timeline());
		return "timeline";
	}
}
